
class Vec2D {
	constructor(public x: number, public y: number) {
	}

	public clone() {
		return new Vec2D(this.x, this.y);
	}

	public copy(src: Vec2D) {
		this.x = src.x;
		this.y = src.y;
	}

	public set(x: number, y: number) {
		this.x = x;
		this.y = y;
	}

	public add(a: Vec2D, b: Vec2D) {
		this.x = a.x + b.x;
		this.y = a.y + b.y;
	}

	public addSelf(b: Vec2D) {
		this.x = this.x + b.x;
		this.y = this.y + b.y;
	}

	public sub(a: Vec2D, b: Vec2D) {
		this.x = a.x - b.x;
		this.y = a.y - b.y;
	}

	public subSelf(b: Vec2D) {
		this.x = this.x - b.x;
		this.y = this.y - b.y;
	}

	public scale(a: Vec2D, factor: number) {
		this.x = a.x * factor;
		this.y = a.y * factor;
	}

	public scaleSelf(factor: number) {
		this.x = this.x * factor;
		this.y = this.y * factor;
	}

	public sqrLength(): number {
		return this.x * this.x + this.y * this.y;
	}

	public length(): number {
		return Math.sqrt(this.sqrLength());
	}

	public normalize(a: Vec2D) {
		let len = a.length();
		this.x = a.x / len;
		this.y = a.y / len;
	}

	public normalizeSelf() {
		let len = this.length();
		this.x = this.x / len;
		this.y = this.y / len;
	}

	public dot(a: Vec2D) {
		return this.x * a.x + this.y * a.y;
	}
}

let GRAVITY = new Vec2D(0,0.1);
let tmpVec1 = new Vec2D(0,0);
let tmpVec2 = new Vec2D(0,0);
class Particle {
	public pos = new Vec2D(0,0);
	public spd = new Vec2D(0,0);
	
	constructor() {
	}

	public draw( ctx: CanvasRenderingContext2D ) {
		ctx.fillStyle = 'black';
		ctx.beginPath();
		ctx.arc(this.pos.x, this.pos.y, 3, 0, 2 * Math.PI, false);
		ctx.fill();
	}

	update(dt: number) {
		tmpVec1.scale(this.spd, dt);
		this.pos.addSelf(tmpVec1);
		tmpVec1.scale(GRAVITY, dt);
		this.spd.addSelf(GRAVITY);
	}
}

class Disc {
	constructor(public pos: Vec2D, public radius: number) {
	}

	public draw( ctx: CanvasRenderingContext2D ) {
		ctx.beginPath();
		ctx.arc(this.pos.x, this.pos.y, this.radius, 0, 2 * Math.PI, false);
		ctx.stroke();
	}
}

class Chain {
	public particles: Array<Particle> = [];
	public discs: Array<Disc> = [];
	
	constructor(startPos: Vec2D, numParticles: number, public linkSize: number) {
		let pos = startPos.clone();
		for (let i = 0; i < numParticles; ++i) {
			let p = new Particle();
			p.pos.copy(pos);
			pos.x += 0;
			this.particles.push(p);
		}
	}

	public draw( ctx: CanvasRenderingContext2D ) {
		ctx.beginPath();
		const startP = this.particles[0].pos;
		ctx.moveTo(startP.x, startP.y);
		for ( let i = 1; i < this.particles.length; ++i ) {
			const p = this.particles[i].pos;
			ctx.lineTo(p.x, p.y);
		}
		ctx.strokeStyle = "black";
		ctx.lineWidth = 2;
		ctx.stroke();

		for ( let p of this.particles ) {
			p.draw(ctx);
		}

		for ( let d of this.discs ) {
			d.draw(ctx);
		}
	}

	public update(dt: number) {
		for ( let j = 0; j < 100; ++j ) {
			for ( let d of this.discs )
			for ( let p of this.particles ) {
				this.processDisc(p, d);
			}

			for ( let i = 1; i < this.particles.length; ++i ) {
				const p1 = this.particles[i-1];
				const p2 = this.particles[i];
				this.processLink(p1, p2);
			}
		}

		for ( let p of this.particles ) {
			p.update(dt);
		}
	}

	processLink(p1: Particle, p2: Particle) {
		tmpVec1.sub(p1.pos, p2.pos);
		let sqrDist = tmpVec1.sqrLength();
		if ( sqrDist > this.linkSize * this.linkSize ) {
			let dist = Math.sqrt(sqrDist);
			let diff = dist - this.linkSize;
			tmpVec1.scaleSelf(1/dist); // normal vec pointing from p2 to p1
			tmpVec2.scale(tmpVec1, diff * -0.5);
			p1.pos.addSelf(tmpVec2);
			p2.pos.subSelf(tmpVec2);

			let relativeSpeed = p1.spd.dot(tmpVec1) - p2.spd.dot(tmpVec1);
			tmpVec2.scale(tmpVec1, relativeSpeed * -0.5);
			p1.spd.addSelf(tmpVec2);
			p2.spd.subSelf(tmpVec2);
		}

	}

	processDisc(p: Particle, d: Disc) {
		tmpVec1.sub(p.pos, d.pos);
		let sqrDist = tmpVec1.sqrLength();
		if ( sqrDist < d.radius * d.radius ) {
			let dist = Math.sqrt(sqrDist);
			let diff = dist - d.radius;
			tmpVec1.scaleSelf(1/dist); // normal vec pointing from d to p
			tmpVec2.scale(tmpVec1, diff);
			p.pos.subSelf(tmpVec2);

			let relativeSpeed = p.spd.dot(tmpVec1);
			if ( relativeSpeed < 0 ) {
				tmpVec2.scale(tmpVec1, relativeSpeed);
				p.spd.subSelf(tmpVec2);
			}
		}
	}
}


function main() {
	let canvasElem = document.getElementById("canvas") as HTMLCanvasElement;
	let canvasCtx = canvasElem.getContext("2d")!;

	let chain = new Chain(new Vec2D(200,100), 300, 20);
	chain.discs.push(new Disc(new Vec2D(100,500), 50));
	chain.discs.push(new Disc(new Vec2D(150,500), 50));
	chain.discs.push(new Disc(new Vec2D(200,500), 50));
	chain.discs.push(new Disc(new Vec2D(250,500), 50));
	//chain.discs.push(new Disc(new Vec2D(300,500), 50));
	chain.discs.push(new Disc(new Vec2D(300,350), 20));

	let mousePos = new Vec2D(0,0);

	let mouseDown = false;
	document.onmousedown = e => mouseDown = true;
	document.onmouseup = e => mouseDown = false;

	canvasElem.onmousemove = ev => {
		mousePos.x = ev.offsetX;
		mousePos.y = ev.offsetY;
	}

	function draw() {
		if ( mouseDown ) {
			chain.particles[0].pos.copy(mousePos);
			chain.particles[0].spd.set(0,0);
		}
		chain.update(1);

		canvasCtx.clearRect(0,0,canvasElem.width, canvasElem.height);
		chain.draw(canvasCtx);
		requestAnimationFrame(draw);
	}

	draw();
}

main();
